

package Interface;

public interface Car {
	public String getBody();

	public String getColour();

	public int getAnnualCost();

}


