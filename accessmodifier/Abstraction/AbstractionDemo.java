package Abstraction;

public class AbstractionDemo {
	public static void main(String args[]) {
		Person student = new Employee("sweety", "Female", 15);
		Person employee = new Employee("mathu", "Female", 255);
		student.work();
		employee.work();
		employee.changeName("suwishana");
		System.out.println(employee.toString());

	}

}


