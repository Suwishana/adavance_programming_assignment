package Constructor;

public class ConstructorDemo {

	public static void main(String[] args) {
		Student suwishana = new Student("suwishana");
		suwishana.printDetails();

		Student thusi = new Student("thusi");
		thusi.printDetails();

		Student mithoon = new Student("mithoon");
		mithoon.printDetails();

	}
}
