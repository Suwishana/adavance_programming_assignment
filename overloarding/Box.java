package overloarding;

public class Box {
	public Box() {
		System.out.println("Empty box!!!");
	}
	public Box(int w,int h) {
		int a=w*h;
		int v=0;
		System.out.println("Area ="+a+",volume="+v);
	}
	public Box (int w,int h,int d) {
		int a=w*h;
		int v=a*d;
		System.out.println("Area="+a+",volume="+v);
	
	}

}
