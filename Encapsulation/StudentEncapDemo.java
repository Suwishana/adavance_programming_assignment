package Encapsulation;

public class StudentEncapDemo {

	public static void main(String[] args) {

		Student studentRaja = new Student();
		studentRaja.setName1("suwishana");
		studentRaja.setAddress1("Colombo");
		studentRaja.setBatch1("CSD-16");
		studentRaja.setAge1(20);

		Student studentRani = new Student();
		studentRani.setName1("Rani");
		studentRani.setAddress1("Jaffna");
		studentRani.setBatch1("CSD-17");
		studentRani.setAge1(18);

		System.out.println(studentRaja.getName11());
		System.out.println(studentRani.getName11() + "-" + studentRani.getAge1());

		System.out.println(studentRaja);
	}

}
