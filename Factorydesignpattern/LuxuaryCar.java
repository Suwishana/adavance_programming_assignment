package Factorydesignpattern;

public class LuxuaryCar extends Car{
	private CarColour colour;
	public LuxuaryCar (CarColour colour) {
		super(CarType.LUXUARY);
		this.colour =colour;
		assembel();
		paint();
	}
	@Override
	void assembel() {
		System.out.println("Assemble LuxuaryCar");
		
	}
	@Override
	void paint() {
		System.out.println("painted colour is"+colour);
	}

}
