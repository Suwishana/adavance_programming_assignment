package Factorydesignpattern;

public class CarFactory {
	public static Car  buildCar(CarType type,CarColour colour) {
		switch (type) {
		case SMALL:
		    return new SmallCar(colour);
		case LUXUARY:
		    return new LuxuaryCar(colour);
		case SEDAN:
		    return new SedanCar(colour);
		}
		return null;
	}

}
