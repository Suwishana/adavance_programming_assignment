package Factorydesignpattern;

public class CarFactoryDemo {
	public static void main(String[]args) {
		CarFactory.buildCar(CarType.LUXUARY,CarColour.BLACK);
		System.out.println("---------------------");
		CarFactory.buildCar(CarType.SMALL,CarColour.WHITE);
		System.out.println("---------------------");
		CarFactory.buildCar(CarType.SEDAN,CarColour.RED);
		
	}
	
	

}
