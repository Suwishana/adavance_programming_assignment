package Factorydesignpattern;

public class SedanCar extends Car {
	private CarColour colour;
	public SedanCar (CarColour colour) {
		super(CarType.SEDAN);
		this.colour =colour;
		assembel();
		paint();
	}
	@Override
	void assembel() {
		System.out.println("Assemble SedanCar");
		
	}
	@Override
	void paint() {
		System.out.println("painted colour is"+colour);
	}

}



