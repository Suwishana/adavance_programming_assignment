package Factorydesignpattern;

public abstract class Car {
	CarType carType;
	abstract void assembel();
	abstract void paint();
	
	public Car(CarType carType) {
		this .carType= carType;
		this.arrangeparts();
	}
	private void arrangeparts() {
		System.out.println("Arranging parts for" + getcarType()+"car");
	}
	public CarType getcarType() {
		return carType;
		
	}
	public void setcarType(CarType carType) {
		this.carType =carType;
	}
	

}
